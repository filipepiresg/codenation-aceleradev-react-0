import React, { Fragment } from "react"

const getMarkes = (str = "", mark) => {
  const indexes = []
  const _str = str.toLowerCase()
  if (mark && _str.includes(mark)) {
    const index = _str.search(mark)
    indexes.push(str.substring(0, index))
    indexes.push(str.substring(index, index + mark.length))
    indexes.push(str.substring(index + mark.length, _str.length))
  }
  return indexes
}

const _renderItemMarked = (indexes, str) => {
  if (!indexes.length) {
    return str
  } else {
    return (
      <Fragment>
        {indexes[0]}
        <mark style={{ backgroundColor: "yellow" }}>{indexes[1]}</mark>
        {indexes[2]}
      </Fragment>
    )
  }
}

const RecipeItem = ({ title, ingredients, mark, thumbnail }) => {
  const indexesTitle = getMarkes(title, mark)
  const indexesIngredients = getMarkes(ingredients, mark)
  return (
    <div className="col-sm-3 mt-4">
      <div className="card">
        <img className="card-img-top img-fluid" src={thumbnail} alt="" />
        <div className="card-body">
          <h5 className="card-title">
            {_renderItemMarked(indexesTitle, title)}
          </h5>
          <p className="card-text">
            <strong>Ingredients: </strong>
            {_renderItemMarked(indexesIngredients, ingredients)}
          </p>
        </div>
      </div>
    </div>
  )
}

export default RecipeItem
