import React, { Component } from "react"
import Navbar from "./Navbar"
import RecipeItem from "./RecipeItem"
import recipes from "../sample_data/recipes.json"

class App extends Component {
  constructor(props) {
    super(props)

    this.recipes = recipes.results
    this.state = {
      searchString: ""
    }
  }

  _handleSearch = e => {
    const { value } = e.target
    this.setState({ searchString: value.toLowerCase() })
  }

  _renderItem = ({ title, ingredients, thumbnail }, index, search) => (
    <RecipeItem
      key={title + index}
      title={title}
      ingredients={ingredients}
      thumbnail={thumbnail}
      mark={search.trim()}
    />
  )

  filterSearch = (title, ingredients, search) =>
    title.toLowerCase().includes(search.trim()) ||
    ingredients.toLowerCase().includes(search.trim())

  render() {
    const { searchString } = this.state
    return (
      <div className="App">
        <Navbar onChangeSearch={this._handleSearch} search={searchString} />
        <div className="container mt-10">
          <div className="row">
            {recipes.resultss &&
              recipes.results
                .filter(({ title, ingredients }) =>
                  this.filterSearch(title, ingredients, searchString)
                )
                .map((item, index) =>
                  this._renderItem(item, index, searchString)
                )}
          </div>
        </div>
      </div>
    )
  }
}

export default App
